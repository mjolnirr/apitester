class InterfacesController < ApplicationController
  INTERFACE = {
    params: [
      {
        name: 'method',
        type: 'enum',
        values: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
      },
      {
        name: 'url',
        type: 'string'
      },
      {
        name: 'request',
        type: 'hash'
      },
      {
        name: 'response',
        type: 'hash'
      },
      {
        name: 'match_types',
        type: 'boolean'
      }
    ]
  }

  def show
    render json: INTERFACE
  end
end
