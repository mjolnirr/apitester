require './lib/airborne.rb'

class MethodsController < ApplicationController
  protect_from_forgery :except => :create

  def create
    method = params[:method]
    url = params[:url]
    request = params[:request] || '{}'
    response = params[:response] || '{}'
    request = '{}' if request.blank?
    response = '{}' if response.blank?

    begin
      match_types = params[:match_type].to_s == 'true'
      request = JSON.parse(request) if request.is_a? String
      response = JSON.parse(response) if response.is_a? String

      request = request.deep_symbolize_keys
      response = response.deep_symbolize_keys

      airborne = AirTest.new

      case method
      when 'GET', 'get'
        airborne.get url, request
        if match_types
          airborne.expect_json_types response
        else
          airborne.expect_json response
        end
      when 'POST', 'post'
        airborne.post url, request
        if match_types
          airborne.expect_json_types response
        else
          airborne.expect_json response
        end
      when 'PUT', 'put', 'PATCH', 'patch'
        airborne.put url, request
        if match_types
          airborne.expect_json_types response
        else
          airborne.expect_json response
        end
      when 'DELETE', 'delete'
        airborne.delete url, request
        if match_types
          airborne.expect_json_types response
        else
          airborne.expect_json response
        end
      end

      render json: {status: 'ok'}
    rescue Exception => e
      backtrace = e.backtrace.join("\n")

      render json: {status: 'error', message: e.message, verbose_message: "#{e.message}\n\n#{backtrace}"}, status: 400
    end
  end
end
