require 'rspec'
require 'airborne'

class AirTest
  include RSpec
  include RSpec::Expectations
  include RSpec::Matchers
  include Airborne
  include Airborne::RequestExpectations

  def expect(val)
    ExpectationTarget.new(val)
  end

  def initialize
    Airborne.configuration.match_expected = true
    Airborne.configuration.match_actual = true
  end
end